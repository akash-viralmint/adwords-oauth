import os
from app import (create_app)

app = create_app(config=os.environ.get('ENV', 'Development'))

if __name__ == '__main__':
    app.run()
