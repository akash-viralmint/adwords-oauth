from flask import Blueprint, current_app, jsonify, redirect, request, session, url_for
from googleads import adwords
from googleads import oauth2
from .model import OAuth

oauth = Blueprint("oauth", __name__, url_prefix="/oauth")


@oauth.route("/login", methods=["GET"])
def login():
    oauth = OAuth(['https://www.googleapis.com/auth/adwords'])
    return redirect(oauth.get_auth_url())


@oauth.route('/callback')
def callback():
    oauth = OAuth(['https://www.googleapis.com/auth/adwords'])
    credentials = oauth.exchange_token(request.args.get('code'))
    session['refresh_token'] = credentials.refresh_token
    return redirect(url_for('oauth.home'))


@oauth.route('/home')
def home():
    oauth = OAuth(['https://www.googleapis.com/auth/adwords'])
    adwords_client = adwords.AdWordsClient(current_app.config['DEVELOPER_TOKEN'], oauth.get_oauth2_client(), current_app.config['USER_AGENT'])
    customer = adwords_client.GetService('CustomerService').get()
    return jsonify(customer=customer['customerId'])
