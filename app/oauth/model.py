from flask import (current_app, session, url_for)
from googleads import oauth2
from oauth2client import client


class OAuth:
    def __init__(self, scopes):
        self.flow = client.OAuth2WebServerFlow(
            client_id=current_app.config['CLIENT_ID'],
            client_secret=current_app.config['CLIENT_SECRET'],
            scope=scopes,
            access_type='offline',
            user_agent=current_app.config['USER_AGENT'],
            redirect_uri=url_for('oauth.callback', _external=True)
        )

    def get_auth_url(self):
        return self.flow.step1_get_authorize_url()

    def exchange_token(self, code):
        return self.flow.step2_exchange(code)

    def get_oauth2_client(self):
        return oauth2.GoogleRefreshTokenClient(
            current_app.config['CLIENT_ID'],
            current_app.config['CLIENT_SECRET'],
            session.get('refresh_token')
        )
