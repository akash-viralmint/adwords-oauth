from flask import (Flask)
from oauth.controller import oauth

__all__ = ['create_app']

DEFAULT_BLUEPRINTS = [
    oauth
]


def create_app(config='Development'):
    """Creates a Flask app with all custom configurations."""
    app = Flask(__name__)

    configure_app(app, config)
    configure_blueprints(app, DEFAULT_BLUEPRINTS)
    return app


def configure_app(app, config):
    """Sample configuration in config.py"""
    app.config.from_object('config.%sConfig' % config)


def configure_blueprints(app, blueprints):
    """Configure blueprints from controllers."""
    for blueprint in blueprints:
        app.register_blueprint(blueprint)
